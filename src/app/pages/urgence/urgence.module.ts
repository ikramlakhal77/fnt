import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UrgencePageRoutingModule } from './urgence-routing.module';

import { UrgencePage } from './urgence.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UrgencePageRoutingModule
  ],
  declarations: [UrgencePage]
})
export class UrgencePageModule {}
