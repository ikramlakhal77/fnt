import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NvcontactPageRoutingModule } from './nvcontact-routing.module';

import { NvcontactPage } from './nvcontact.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NvcontactPageRoutingModule
  ],
  declarations: [NvcontactPage]
})
export class NvcontactPageModule {}
