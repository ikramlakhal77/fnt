import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NvcontactPage } from './nvcontact.page';

const routes: Routes = [
  {
    path: '',
    component: NvcontactPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NvcontactPageRoutingModule {}
