import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'slides',
    pathMatch: 'full'
  },
  {
    path: 'slides',
    loadChildren: () => import('./slides/slides.module').then( m => m.SlidesPageModule)
  },
  {
    path: 'dossier',
    loadChildren: () => import('./dossier/dossier.module').then( m => m.DossierPageModule)
  },
  {
    path: 'contact',
    loadChildren: () => import('./pages/contact/contact.module').then( m => m.ContactPageModule)
  },
  {
    path: 'propos',
    loadChildren: () => import('./pages/propos/propos.module').then( m => m.ProposPageModule)
  },
  {
    path: 'rendez-vous',
    loadChildren: () => import('./pages/rendez-vous/rendez-vous.module').then( m => m.RendezVousPageModule)
  },
  {
    path: 'urgence',
    loadChildren: () => import('./pages/urgence/urgence.module').then( m => m.UrgencePageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
