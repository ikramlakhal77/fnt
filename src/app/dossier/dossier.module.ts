import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DossierPageRoutingModule } from './dossier-routing.module';

import { DossierPage } from './dossier.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DossierPageRoutingModule
  ],
  declarations: [DossierPage]
})
export class DossierPageModule {}
